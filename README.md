# Tuya MQTT client

This is a fully functional MQTT client based on paho-mqtt library, which implements basic functionality for Tuya MQTT communication, such as reporting and receiving device properties from Tuya cloud. Client is implemented as TuyaClient class inside tuya_mqtt_client.py as an importable python module, example codes for usage are provided in example directory.

## Requirements

As this program is based on paho-mqtt library, you need to install paho-mqtt library using the package manager [pip](https://pip.pypa.io/en/stable/).

```bash
pip install paho-mqtt
```

## Usage
Client can be imported as a python module as such:
```python
import tuya_mqtt_client
```
Client class needs to be initialized with device ID and device secret ID assigned to device from Tuya cloud and filepath to TLS certificate in .pem format:
```python
client  = tuya_mqtt_client.TuyaClient("device_ID", "device_secret_ID", "/filepath_to_TLS_certificate")
```
To connect and authenticate client to Tuya cloud, client_init() method must be called:
```python
client.client_init()
```
To start loop function for reading the receive and send buffers, and processing messages, loop_start() method must be called. To stop the loop, call loop_stop(). Alternatively loop_forever method can be called to keep the loop running forever.

To report properties to Tuya cloud, store them in python dictionary, and report them with report_properties(msgId, data) method, which takes message ID and data stored in python dictionary as an argument.
```python
data_dict = {
        "switch_1": True
    }
client.report_properties(msgId="123", data=data_dict)
```
As of now, incoming messages are simply printed to console.
