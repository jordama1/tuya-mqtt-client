import paho.mqtt.client as mqtt

import json
import hmac
import hashlib
import binascii
import base64
import time
import json
import ssl


class TuyaClient:    

    #callback that triggers on conection, subscribe to topics here, so the client resubscribes to topics upon reconnection
    def on_connect(self, client, userdata, flags, rc):
        print("Connected with result code "+str(rc))
        client.subscribe("tylink/"+self.device_ID+"/thing/model/get_response", qos=1)
        client.subscribe("tylink/"+self.device_ID+"/thing/property/set", qos=1)
        client.subscribe("tylink/"+self.device_ID+"/thing/action/execute", qos=1)
        client.subscribe("tylink/"+self.device_ID+"/ext/config/get_response", qos=1)
        client.subscribe("tylink/"+self.device_ID+"/ext/time/response", qos=1)

    #callback for handling incoming messages
    def on_message(self, client, userdata, msg):
        self.incoming_message_payload = json.loads(msg.payload)
        print(self.incoming_message_payload)
        #return(self.incoming_message_payload)

    #callback for viewing logs    
    def on_log(self, client, userdata, level, buf):
        #uncomment next line to view logs
        print (buf)
        #pass

    #constructor, initialize device ID, device secret ID, and path to TLS certificate
    def __init__(self, device_ID, device_secret_ID, cert_path):
        self.device_ID = device_ID
        self.device_secret_ID = device_secret_ID
        self.cert_path = cert_path
    
    #intialize mqtt client
    def client_init(self):
        self.client = mqtt.Client(client_id="tuyalink_"+self.device_ID)
        self.timestamp = int(time.time())

        #content string - deviceId=${DeviceID},timestamp=${10-digit current timestamp},secureMode=1,accessType=1
        content_string = "deviceId="+self.device_ID+",timestamp="+str(self.timestamp)+",secureMode=1,accessType=1"

        #sha256 hash from content_string with device_secret_ID as a key
        digest = hmac.new(self.device_secret_ID.encode('UTF-8'), content_string.encode('UTF-8'), hashlib.sha256)
        signature = digest.hexdigest()

        #setup username and password
        #username string - ${DeviceID}|signMethod=hmacSha256,timestamp=${10-digit current timestamp},secureMode=1,accessType=1;
        self.client.username_pw_set(self.device_ID+"|signMethod=hmacSha256,timestamp="+str(self.timestamp)+",secureMode=1,accessType=1", password=signature)

        #enable TLS, set certificate
        self.client.tls_set(ca_certs=self.cert_path, tls_version=ssl.PROTOCOL_TLSv1_2)

        #connect client to tuya server
        self.client.connect('m1.tuyaeu.com', port=8883, keepalive=60)

        #connect callbacks
        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message
        self.client.on_log = self.on_log
        
        time.sleep(2)

    def report_properties(self, msgId, data):
        #encapsulate data dictionary into properties dictionary
        properties = {
            "msgId":str(msgId),
            "time":self.timestamp,
            "data": data
        }
        #convert properties to json payload
        payload = json.dumps(properties)
        #publish payload
        ret_pub = self.client.publish(str('tylink/'+self.device_ID+'/thing/property/report'),payload = payload, qos = 1)
    def get_model(self, msgId, format):
        payload_dict = {
            "msgId":"45lkj3551234***",
  	        "time":self.timestamp,
  	        "data":{
    	    "format":format
        }
        }
        payload = json.dumps(payload_dict)
        self.client.publish("tylink/"+self.device_ID+"/thing/model/get", payload=payload, qos=1)
    def action_response(self, msgId, action_code, output_parameters, status_code):
        msgBody = {
            "msgId":str(msgId),
            "time":self.timestamp,
            "code":status_code,
            "data":{
                "actionCode":action_code,
                "outputParams":output_parameters
            }
        }
        payload = json.dumps(msgBody)
        ret_pub = self.client.publish(str('tylink/'+self.device_ID+'/thing/action/execute_response'),payload = payload, qos = 1)
    def event_report (self, msgId, event_code, event_time, output_parameters):
        msgBody = {
            "msgId":str(msgId),
            "time":self.timestamp,
            "data":{
                "eventCode":event_code,
                "eventTime":event_time,
                "outputParams":output_parameters
            }
        }
        payload = json.dumps(msgBody)
        ret_pub = self.client.publish(str('tylink/'+self.device_ID+'/thing/event/trigger'),payload = payload, qos = 1)
    def bulk_report(self, msgId, properties, events, subdevices):
        msgBody = {
            "msgId":str(msgId),
            "time":self.timestamp,
            "data":{
                "properties":properties,
                "events":events
            },
            "subDevices":[
                subdevices
            ]
        }
        print (msgBody)
        payload = json.dumps(msgBody)
        print(payload)
        #ret_pub = self.client.publish(str('tylink/'+self.device_ID+'/thing/data/batch_report'),payload = payload, qos = 1)
    def report_history(self):
        pass
    def get_config_file(self, msgId):
        config_timestamp = int(time.time())
        msgBody = {
            "msgId":msgId,
            "time":config_timestamp,
            "data":{
                "bizType":"PRODUCT_FILE"
            }
        }
        payload = json.dumps(msgBody)
        print(payload)
        ret_pub = self.client.publish(str('tylink/'+self.device_ID+'/ext/config/get'),payload = payload, qos = 1)
    def NTP_time_request(self, msgId):
        unix_timestamp = int(1000*time.time())
        msgBody = {
            "msgId":msgId,
            "time":unix_timestamp,
            "data":{
                "bizType":"NTP",
                "dst":unix_timestamp
            }
        }
        payload = json.dumps(msgBody)
        #print(payload)
        ret_pub = self.client.publish(str('tylink/'+self.device_ID+'/ext/time/request'),payload = payload, qos = 1)
        
    def DST_time_request(self, msgId, timezoneId):
        unix_timestamp = int(1000*time.time())
        msgBody = {
            "msgId":msgId,
            "time":unix_timestamp,
            "data":{
                "bizType":"DST",
                "timezoneId":timezoneId
            }
        }
        payload = json.dumps(msgBody)
        #print(payload)
        ret_pub = self.client.publish(str('tylink/'+self.device_ID+'/ext/time/request'),payload = payload, qos = 1)
    def loop_forever(self):
        self.client.loop_forever()
    def loop_start(self):
        self.client.loop_start()
    def loop_stop(self):
        self.client.loop_stop()
        