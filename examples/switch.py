import sys
import os
import time

current = os.path.dirname(os.path.realpath(__file__))

parent = os.path.dirname(current)

sys.path.append(parent)
#sys.path.append("..") # Adds higher directory to python modules path.


import tuya_mqtt_client

if __name__ == '__main__':
    client  = tuya_mqtt_client.TuyaClient("device_ID", "device_secret_ID", parent+"/Tuya_CA_cert.pem")
    client.client_init()
    
    #client.loop_start()

    data_dict = {
        "switch_1": True
    }
    
    client.report_properties("123", data_dict)

    client.loop_forever()

    #client.get_model("123", "simple")
    
    
    #time.sleep(60)

    #client.loop_stop()
